#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "indices.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <Eigen/Dense>
using namespace Eigen;

void split_token(std::string& s, std::vector<std::string>& v, std::string& c);
std::vector<Asset> parse_assets(char* filename,int &num);
MatrixXd parse_corrlmatrix(char* filename,int num);
double unrest_optimal(Portfolio &P,double &ret);
double rest_optimal(Portfolio &P,double &ret);
#endif
