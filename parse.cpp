#include "indices.hpp"
#include "portfolio.hpp"
#include "parse.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <Eigen/Dense>
using namespace Eigen;

void split_token(std::string& s, std::vector<std::string>& v, std::string& c){
  std::string::size_type pos_pre, pos_post;
  pos_post = s.find(c);
  pos_pre = 0;
  while(std::string::npos != pos_post)
  {
    v.push_back(s.substr(pos_pre, pos_post-pos_pre));

    pos_pre = pos_post + c.size();
    pos_post = s.find(c, pos_pre);
  }
  if(pos_pre != s.length()){
    v.push_back(s.substr(pos_pre));
  }
}
std::vector<Asset> parse_assets(char* filename,int &num){
  std::vector<Asset> A;
  std::ifstream ifs(filename);
  if(!ifs.is_open()){
    perror("Could not open the file.");
        exit(EXIT_FAILURE);
  }
  std::string line;
  std::string c = ",";
  while(std::getline(ifs,line)){
    std::vector<std::string> t;
    split_token(line,t,c);
    if(t.size() != 3){
      perror("Wrong format.");//error case: wrong number in formats
          exit(EXIT_FAILURE);
    }
    Asset temp;
    temp.name = t[0];
    if(atof(t[1].c_str())==0.0 || atof(t[2].c_str())==0.0){
      perror("Detecting non-numeric value.");//error case: non-numeric value
          exit(EXIT_FAILURE);
    }
    temp.avr = atof(t[1].c_str());
    temp.sigma = atof(t[2].c_str());
    num++;
    A.push_back(temp);
  }
if(num == 0){
    perror("Empty file.");//error case: empty file
    exit(EXIT_FAILURE);
  }
  return A;
}
//Read correlation matrix
MatrixXd parse_corrlmatrix(char* filename,int num){
  std::ifstream ifs(filename);
  if(!ifs.is_open()){
    perror("Could not open correlation file");
        exit(EXIT_FAILURE);
  }

  MatrixXd corr(num,num);
  std::string line;
  std::string c = ",";
  int i=0;
  while(std::getline(ifs,line)){
    if(i == num){
      perror("Dimension of correlation matrix does not match with  the number of assets");//error case:handle larger matrix while less assets
      exit(EXIT_FAILURE);
    }
    std::vector<std::string> t;
    split_token(line,t,c);
    if(t.size() != (size_t)num){
 perror("Dimension of correlation matrix does not match with the number of assets");//error case :handle different dimensions
      exit(EXIT_FAILURE);
    }
    for(int j=0;j<num;j++){
      if(atof(t[j].c_str())==0.0){
        perror("Correlation matrix has non-numeric value");//error case:handle non-numeric value
            exit(EXIT_FAILURE);
      }
      corr(i,j) = atof(t[j].c_str());
    }
    i++;
  }
  if(i == 0){
    perror("Empty file: Correlation file");//error case:handle empty file
    exit(EXIT_FAILURE);
  }
  for(int i=0;i<num;i++){
    for(int j=0;j<=i;j++){
      if(fabs(corr(i,j) - corr(j,i)) > 0.0001 || fabs(corr(i,j)) > 1.0001){
        perror("Wrong mathematical decimal format");//error case:handle wrong format
        exit(EXIT_FAILURE);
 }
  }
  return corr;
}

//unrestricted portfolio
double unrest_optimal(Portfolio &P,double &ret){
  int n = P.n;
  MatrixXd A1 = MatrixXd::Ones(1,n);
  MatrixXd A2(1,n);
  for(int i=0;i<n;i++){
    A2(i) = P.A[i].avr;
  }
  MatrixXd A(2,n);
  A<<A1,A2;
  MatrixXd b1 = MatrixXd::Zero(n,1);
  MatrixXd b2(2,1);
  b2<<1,ret;
  MatrixXd B(n+2,1);
  B<<b1,b2;
  MatrixXd cov(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      cov(i,j) = P.A[i].sigma * P.corr(i,j) * P.A[j].sigma;
    }
  }
  MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd K(n+2,n+2);
  K<<cov,A.transpose(),A,O;
  VectorXd X(n+2,1);
  X = K.fullPivHouseholderQr().solve(B);
  P.weight = X.head(n);
  P.cal_sigma();
  return P.sigma;
}
//restricted portfolio
double rest_optimal(Portfolio &P,double &ret){
  int n = P.n;
  MatrixXd A1 = MatrixXd::Ones(1,n);
  MatrixXd A2(1,n);
  for(int i=0;i<n;i++){
    A2(i) = P.A[i].avr;
  }
  MatrixXd A(2,n);
  A<<A1,A2;
  MatrixXd b1 = MatrixXd::Zero(n,1);
  MatrixXd b2(2,1);
  b2<<1,ret;
  MatrixXd B(n+2,1);
  B<<b1,b2;
  MatrixXd cov(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      cov(i,j) = P.A[i].sigma * P.corr(i,j) * P.A[j].sigma;
    }
  }
 MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd K(n+2,n+2);
  K<<cov,A.transpose(),A,O;
  VectorXd X(n+2,1);
  X = K.fullPivHouseholderQr().solve(B);
  MatrixXd A_A = A;
  MatrixXd B_B = B;
  VectorXd X_X = X;
  for(int i=1;;i++){
    int flag = 1;
    MatrixXd C;
    int k=0;
    for(int j=0;j<n;j++){
      if(X_X(j) < 0){
        MatrixXd M  = MatrixXd::Zero(n,1);
        M(j,0) = 1;
        MatrixXd temp = C;
        C.resize(n,k+1);
        if(k==0){
          C<<M;
        }
        else{
          C<<temp,M;
        }
        flag = 0;
        k++;
      }
    }
    if(flag == 1){
      break;
    }
    MatrixXd M;
    M = A_A;
    A_A.resize(M.rows() + C.cols(),n);
    A_A<<M,C.transpose();
    M = B_B;
    B_B.resize(M.rows() + C.cols(),1);
    B_B<<M,MatrixXd::Zero(C.cols(),1);
    MatrixXd O_O = MatrixXd::Zero(A_A.rows(),A_A.rows());
    MatrixXd K_K(cov.rows() + A_A.rows(),cov.rows() + A_A.rows());
    K_K<<cov,A_A.transpose(),A_A,O_O;
    X_X = K_K.fullPivHouseholderQr().solve(B_B);
  }
 P.weight = X_X.head(n);
  P.cal_sigma();
  return P.sigma;
}


