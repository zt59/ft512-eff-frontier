#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__

#include "indices.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

class Portfolio {
  public:
    double ror;
    double sigma;
    MatrixXd corr;
    int n;
    VectorXd weight;
    std::vector<Asset> A;
  public:
    Portfolio(){}
    void cal_ror(){
      int sum = 0;
      for(int i=0;i<n;i++){
        sum += weight(i)*A[i].avr;
      }
      ror = sum;
    }
    void cal_sigma(){
      double sum = 0;
      for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
          sum += weight(i)*weight(j)*corr(i,j)*A[i].sigma*A[j].sigma;
        }
      }
      sigma = std::sqrt(sum);
    }
    ~Portfolio(){}
    };

#endif


