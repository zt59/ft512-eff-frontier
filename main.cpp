#include "indices.hpp"
#include "parse.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
#include <iomanip>
#include <unistd.h>
using namespace Eigen;

int main(int argc, char ** argv){
  if (argc != 3 && argc !=4) {
    fprintf(stderr, "Wrong number of arguments");
    return EXIT_FAILURE;
  }
  if(argc ==4 && strlen(argv[1]) != 2){
      std::cerr<<"Option size or position is wrong";
      return EXIT_FAILURE;
  }
//check whether "-r" is passed
  int opt;
  bool r = false;
  while ((opt = getopt(argc, argv, "r")) != -1){
    switch(opt){
      case 'r':
        r = true;
        break;
      case '?':
        std::cerr<<"Option is wrong";
        return EXIT_FAILURE;
    }
  }
 int num = 0;
  std::vector<Asset> A;
  MatrixXd corr;
  if(r == true){
    A = parse_assets(argv[2],num);
    corr = parse_corrlmatrix(argv[3],num);
  }
  else{
    A = parse_assets(argv[1],num);
    corr = parse_corrlmatrix(argv[2],num);
  }


  Portfolio P;
  P.n = num;
  P.A = A;
  P.corr = corr;
 std::cout<<"ROR,volatility"<<std::endl;
  std::cout.setf(std::ios::fixed);
  if(r == false){
    for(double l = 0.01;l <= 0.265;l += 0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<l*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<unrest_optimal(P,l)*100<<"%"<<std::endl;
    }
  }

  if(r == true){
    for(double l = 0.01;l <= 0.265;l += 0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<l*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<rest_optimal(P,l)*100<<"%"<<std::endl;
    }
  }
  std::cout.unsetf(std::ios::fixed);
  return EXIT_SUCCESS;
}

